package com.company;


import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> availableSpaceships = new ArrayList<>();
        int indexNow = -1;

        for (int i = 0; i < ships.size(); i++) {
            if (availableSpaceships.size() == 0) {
                if (ships.get(i).getFirePower() > 0) {
                    availableSpaceships.add(ships.get(i));
                    indexNow++;
                }
            } else {
                if (ships.get(i).getFirePower() > availableSpaceships.get(indexNow).getFirePower()) {
                    availableSpaceships.add(ships.get(i));
                    indexNow++;
                }
            }
        }

        if (availableSpaceships.size() == 0) {
            return null;
        } else {
            return availableSpaceships.get(availableSpaceships.size() - 1);
        }
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for (Spaceship spaceship : ships) {
            if (spaceship.getName().equals(name)) {
                return spaceship;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> availableSpaceships = new ArrayList<>();

        for (Spaceship spaceship : ships) {
            if (spaceship.getFirePower() <= 0) {
                availableSpaceships.add(spaceship);
            }
        }
        return availableSpaceships;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> availableSpaceships = new ArrayList<>();

        for (Spaceship spaceship : ships) {
            if (spaceship.getCargoSpace() >= cargoSize) {
                availableSpaceships.add(spaceship);
            }
        }

        return availableSpaceships;
    }
}
