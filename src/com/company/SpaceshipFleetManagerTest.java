package com.company;

import java.util.ArrayList;

public class SpaceshipFleetManagerTest {

    SpaceshipFleetManager center;
    ArrayList<Spaceship> testShips = new ArrayList<>();


    public static void main(String[] args) {
        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest(new CommandCenter());
        double totalScore = 0;
        double rightAnswer = 0.45;

        boolean test1 = spaceshipFleetManagerTest.getAllCivilianShips_peacefulShips_returnNotEmptyShips();
        if(test1){
            System.out.println("All good! / really peaceful ships");
            totalScore += rightAnswer;
        }else{
            System.out.println("Test 1 failed! / really peaceful ships");
        }

        boolean test2 = spaceshipFleetManagerTest.getAllCivilianShips_peacefulShips_returnEmptyShips();
        if(test2){
            System.out.println("All good! / empty peaceful ships");
            totalScore += rightAnswer;
        }else{
            System.out.println("Test 2 failed! / empty peaceful ships");
        }

        System.out.println();

        boolean test3 = spaceshipFleetManagerTest.getMostPowerfulShip_mostPowerfulShip_returnShip();
        if(test3){
            System.out.println("All good! / really powerful ship");
            totalScore += rightAnswer;
        }else{
            System.out.println("Test 2 failed! / really powerful ship");
        }

        boolean test4 = spaceshipFleetManagerTest.getMostPowerfulShip_mostPowerfulShip_returnShipWithRightName();
        if(test4){
            System.out.println("All good! / really powerful ship with right name");
            totalScore += rightAnswer;
        }else{
            System.out.println("Test 2 failed! / really powerful ship with right name");
        }

        boolean test5 = spaceshipFleetManagerTest.getMostPowerfulShip_shipWithoutPower_returnNull();
        if(test5){
            System.out.println("All good! / ship without power");
            totalScore += rightAnswer;
        }else{
            System.out.println("Test 2 failed! / ship without power");
        }

        System.out.println();

        boolean test6 = spaceshipFleetManagerTest.getShipByName_uniqueName_returnShipWithRightName();
        if(test6){
            System.out.println("All good! / ship with unique name");
            totalScore += rightAnswer;
        }else{
            System.out.println("Test 2 failed! / ship with unique name");
        }

        boolean test7 = spaceshipFleetManagerTest.getShipByName_uniqueName_returnNull();
        if(test7){
            System.out.println("All good! / ship with unsuitable name");
            totalScore += rightAnswer;
        }else{
            System.out.println("Test 2 failed! / ship with unsuitable name");
        }

        System.out.println();

        boolean test8 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_accommodatedShips_returnArrayList();
        if(test8){
            System.out.println("All good! / ship with enough cargo space");
            totalScore += rightAnswer;
        }else{
            System.out.println("Test 2 failed! / ship without enough cargo space");
        }

        boolean test9 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_accommodatedShips_returnEmptyShips();
        if(test9){
            System.out.println("All good! / ship with unsuitable cargo space");
            totalScore += rightAnswer;
        }else{
            System.out.println("Test 2 failed! / ship with unsuitable cargo space");
        }

        System.out.println();
        System.out.println("TOTAL SCORE: " + Math.floor(totalScore));
    }

    //ТЕСТ корабли с грузом пустые
    private boolean getAllShipsWithEnoughCargoSpace_accommodatedShips_returnEmptyShips(){
        testShips.add(new Spaceship("10", 1, 10, 10));
        testShips.add(new Spaceship("11", 1, 11, 10));
        testShips.add(new Spaceship("12", 1, 12, 10));
        testShips.add(new Spaceship("13", 1, 13, 10));

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testShips, 15);

        if (result.size() != 0) {
            testShips.clear();
            return false;
        }
        testShips.clear();
        return true;
    }

    //ТЕСТ корабли с реально большими трюмами
    private boolean getAllShipsWithEnoughCargoSpace_accommodatedShips_returnArrayList(){
        int cargoSpace = 10;

        testShips.add(new Spaceship("10", 0, cargoSpace, 10));
        testShips.add(new Spaceship("11", 1, 15, 10));
        testShips.add(new Spaceship("12", 0, cargoSpace, 10));
        testShips.add(new Spaceship("13", 1, 15, 10));

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testShips, cargoSpace);

        if(result.size() == 0){
            return false;
        }

        for(Spaceship spaceship: result){
            if(spaceship.getCargoSpace() < cargoSpace){
                testShips.clear();
                return false;
            }
        }
        testShips.clear();
        return true;
    }

    //TECT корабль по имени null
    private boolean getShipByName_uniqueName_returnNull(){
        testShips.add(new Spaceship("10", 0, 10, 10));
        testShips.add(new Spaceship("11", 1, 10, 10));
        testShips.add(new Spaceship("12", 0, 10, 10));
        testShips.add(new Spaceship("13", 1, 10, 10));

        Spaceship result = center.getShipByName(testShips, "14");

        if(result != null){
            testShips.clear();
            return false;
        }

        testShips.clear();
        return true;
    }

    //TECT корабль с уникальным именем
    private boolean getShipByName_uniqueName_returnShipWithRightName(){
        String name = "12";

        testShips.add(new Spaceship("10", 0, 10, 10));
        testShips.add(new Spaceship("11", 1, 10, 10));
        testShips.add(new Spaceship(name, 0, 10, 10));
        testShips.add(new Spaceship("13", 1, 10, 10));

        Spaceship result = center.getShipByName(testShips, name);

        if(result == null){
            testShips.clear();
            return false;
        }

        if(!result.getName().equals(name)){
            testShips.clear();
            return false;
        }

        testShips.clear();
        return true;
    }


    //TECT мощный корабль with right name
    private boolean getMostPowerfulShip_mostPowerfulShip_returnShipWithRightName(){
        int power = 5;

        testShips.add(new Spaceship("10", power, 10, 10));
        testShips.add(new Spaceship("11", 1, 10, 10));
        testShips.add(new Spaceship("12", 0, 10, 10));
        testShips.add(new Spaceship("13", power, 10, 10));

        Spaceship result = center.getMostPowerfulShip(testShips);

        if(result.getFirePower() != power){
            testShips.clear();
            return false;
        }

        if(!result.getName().equals("10")){
            testShips.clear();
            return false;
        }

        testShips.clear();
        return true;
    }

    //TECT мощный корабль которого не существует
    private boolean getMostPowerfulShip_shipWithoutPower_returnNull(){
        testShips.add(new Spaceship("10", 0, 10, 10));
        testShips.add(new Spaceship("11", 0, 10, 10));
        testShips.add(new Spaceship("12", 0, 10, 10));
        testShips.add(new Spaceship("13", 0, 10, 10));

        Spaceship result = center.getMostPowerfulShip(testShips);

        if(result != null) {
            testShips.clear();
            return false;
        }

        testShips.clear();
        return true;
    }

    //TECT реально мощный корабль
    private boolean getMostPowerfulShip_mostPowerfulShip_returnShip(){
        int power = 5;

        testShips.add(new Spaceship("10", 2, 10, 10));
        testShips.add(new Spaceship("11", 1, 10, 10));
        testShips.add(new Spaceship("12", 0, 10, 10));
        testShips.add(new Spaceship("13", power, 10, 10));

        Spaceship result = center.getMostPowerfulShip(testShips);

        if(result.getFirePower() != power){
            testShips.clear();
            return false;
        }

        testShips.clear();
        return true;
    }

    //ТЕСТ мирные корабли не пустые
    private boolean getAllCivilianShips_peacefulShips_returnNotEmptyShips(){
        testShips.add(new Spaceship("10", 0, 10, 10));
        testShips.add(new Spaceship("11", 1, 10, 10));
        testShips.add(new Spaceship("12", 0, 10, 10));
        testShips.add(new Spaceship("13", 1, 10, 10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testShips);

        if(result.size() == 0){
            return false;
        }

        for(Spaceship spaceship: result){
            if(spaceship.getFirePower() != 0){
                testShips.clear();
                return false;
            }
        }
        testShips.clear();
        return true;
    }

    //ТЕСТ корабли мирные пустые
    private boolean getAllCivilianShips_peacefulShips_returnEmptyShips(){
        testShips.add(new Spaceship("10", 1, 10, 10));
        testShips.add(new Spaceship("11", 1, 10, 10));
        testShips.add(new Spaceship("12", 1, 10, 10));
        testShips.add(new Spaceship("13", 1, 10, 10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testShips);

        if(result.size() != 0){
            testShips.clear();
            return false;
        }
        testShips.clear();
        return true;
    }

    public SpaceshipFleetManagerTest(SpaceshipFleetManager center) {
        this.center = center;
    }
}
