package com.company;

import org.junit.jupiter.api.*;
import java.util.ArrayList;


public class SpaceshipFleetManagerTestWithLibrary {

    static SpaceshipFleetManager center = new CommandCenter();
    static ArrayList<Spaceship> testShips = new ArrayList<>();
    static boolean flag = true;

    @AfterEach
    void clearTestData(){
        testShips.clear();
        flag = true;
    }

    @DisplayName("Корабли с неподходящим размером грузового трюма")
    @Test
    void getAllShipsWithEnoughCargoSpace_accommodatedShips_returnEmptyShips(){
        testShips.add(new Spaceship("10", 1, 10, 10));
        testShips.add(new Spaceship("11", 1, 11, 10));
        testShips.add(new Spaceship("12", 1, 12, 10));
        testShips.add(new Spaceship("13", 1, 13, 10));

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testShips, 15);

        Assertions.assertEquals(0, result.size());
    }

    @DisplayName("Корабли с реально болшьими трюмами")
    @Test
    void getAllShipsWithEnoughCargoSpace_accommodatedShips_returnArrayList(){
        int cargoSpace = 10;

        testShips.add(new Spaceship("10", 0, cargoSpace, 10));
        testShips.add(new Spaceship("11", 1, 15, 10));
        testShips.add(new Spaceship("12", 0, cargoSpace, 10));
        testShips.add(new Spaceship("13", 1, 15, 10));

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testShips, cargoSpace);

       if(result.size() == 0){
           flag = false;
       }

        for(Spaceship spaceship: result){
            if (spaceship.getCargoSpace() < cargoSpace) {
                flag = false;
                break;
            }
        }

        Assertions.assertTrue(flag);
    }

    @DisplayName("Корабли с неподходящими именами")
    @Test
    void getShipByName_uniqueName_returnNull(){
        testShips.add(new Spaceship("10", 0, 10, 10));
        testShips.add(new Spaceship("11", 1, 10, 10));
        testShips.add(new Spaceship("12", 0, 10, 10));
        testShips.add(new Spaceship("13", 1, 10, 10));

        Spaceship result = center.getShipByName(testShips, "14");

        Assertions.assertNull(result);
    }

    @DisplayName("Корабль с уникальным именем")
    @Test
    void getShipByName_uniqueName_returnShipWithRightName(){
        String name = "12";

        testShips.add(new Spaceship("10", 0, 10, 10));
        testShips.add(new Spaceship("11", 1, 10, 10));
        testShips.add(new Spaceship(name, 0, 10, 10));
        testShips.add(new Spaceship("13", 1, 10, 10));

        Spaceship result = center.getShipByName(testShips, name);

        if(result == null || !result.getName().equals(name)){
            flag = false;
        }

        Assertions.assertTrue(flag);
    }

    @DisplayName("Мощный корабль с подходящим именем")
    @Test
    void getMostPowerfulShip_mostPowerfulShip_returnShipWithRightName(){
        int power = 5;

        testShips.add(new Spaceship("10", power, 10, 10));
        testShips.add(new Spaceship("11", 1, 10, 10));
        testShips.add(new Spaceship("12", 0, 10, 10));
        testShips.add(new Spaceship("13", power, 10, 10));

        Spaceship result = center.getMostPowerfulShip(testShips);

        if(result.getFirePower() != power || !result.getName().equals("10")){
            flag = false;
        }

        Assertions.assertTrue(flag);
    }

    @DisplayName("Мощный корабль которого не существует")
    @Test
    void getMostPowerfulShip_shipWithoutPower_returnNull(){
        testShips.add(new Spaceship("10", 0, 10, 10));
        testShips.add(new Spaceship("11", 0, 10, 10));
        testShips.add(new Spaceship("12", 0, 10, 10));
        testShips.add(new Spaceship("13", 0, 10, 10));

        Spaceship result = center.getMostPowerfulShip(testShips);

        Assertions.assertNull(result);
    }

    @DisplayName("Реально мощный корабль")
    @Test
    void getMostPowerfulShip_mostPowerfulShip_returnShip(){
        int power = 5;

        testShips.add(new Spaceship("10", 2, 10, 10));
        testShips.add(new Spaceship("11", 1, 10, 10));
        testShips.add(new Spaceship("12", 0, 10, 10));
        testShips.add(new Spaceship("13", power, 10, 10));

        Spaceship result = center.getMostPowerfulShip(testShips);

        Assertions.assertEquals(power, result.getFirePower());
    }

    @DisplayName("Не пустые мирные корабли")
    @Test
    void getAllCivilianShips_peacefulShips_returnNotEmptyShips(){
        testShips.add(new Spaceship("10", 0, 10, 10));
        testShips.add(new Spaceship("11", 1, 10, 10));
        testShips.add(new Spaceship("12", 0, 10, 10));
        testShips.add(new Spaceship("13", 1, 10, 10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testShips);

        if(result.size() == 0){
            flag = false;
        }

        for(Spaceship spaceship: result){
            if (spaceship.getFirePower() != 0) {
                flag = false;
                break;
            }
        }

        Assertions.assertTrue(flag);
    }

    @DisplayName("Мирные пустые корабли")
    @Test
    void getAllCivilianShips_peacefulShips_returnEmptyShips(){
        testShips.add(new Spaceship("10", 1, 10, 10));
        testShips.add(new Spaceship("11", 1, 10, 10));
        testShips.add(new Spaceship("12", 1, 10, 10));
        testShips.add(new Spaceship("13", 1, 10, 10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testShips);

        Assertions.assertEquals(0, result.size());
    }
}
